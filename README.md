### 项目为SpringBoot的demo项目

### 已完成的试验

* 构建RESTful API与单元测试
* 使用Swagger2构建强大的API文档
* JSR-303实现请求参数校验
* Swagger接口分类与各元素排序问题详解
* Swagger静态文档的生成


### Q&A
* Q: ch.netzwerg:paleo-core:0.10.2 and nl.jworks.markdown_to_asciidoc:markdown_to_asciidoc:1.0 not foud
* A: [solve](https://github.com/Swagger2Markup/swagger2markup/issues/300)

### 参考项目

* [spring-boot](http://blog.didispace.com/spring-boot-learning-21-2-5/)
* [Swagger2Markup Documentation](http://swagger2markup.github.io/swagger2markup/1.3.1/#_release_2)
* [Swagger2环境配置与使用](https://segmentfault.com/a/1190000010911014)

