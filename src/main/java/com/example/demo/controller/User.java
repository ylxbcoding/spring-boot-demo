package com.example.demo.controller;

import  lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
public class User {
    private long id;

    @NotEmpty
    private String name;

    @NotNull
    @Max(100)
    @Min(10)
    private Integer age;
}
